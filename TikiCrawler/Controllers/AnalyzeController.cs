﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TikiCrawler.Models;

namespace TikiCrawler.Controllers
{
    public class AnalyzeController : Controller
    {
        // GET: Analyze
        public ActionResult Index()
        {
            return View();
        }

        // get name product from web client
        [HttpGet]
        public ActionResult ViewProductWithKeywordName(string keyword = "Iphone 6S")
        {
            List<string> badcomments = new List<string>()
            {
                "xấu", "sai yêu cầu", "chất lượng kém", "chất lượng tệ", ""
            };
            List<string> goodcomments = new List<string>()
            {
                "mẫu mã đẹp", "đúng yêu cầu", "chất lượng tốt"
            };

            NoSql db = new NoSql();
            List<Product> list = db.FindAllProductByName(keyword);
            foreach (Product item in list)
            {
                if (item.ReviewPoint < 4.5)
                {
                    list.Remove(item);
                }
                foreach (Comment cm in item.Comments)
                {
                    foreach (string filter in badcomments)
                    {
                        if (cm.Title.Contains(filter) || cm.Decription.Contains(filter))
                        {
                            list.Remove(item);
                            break;
                        }
                    }
                }
            }
            ViewBag.Title = "Best product";
            ViewData["GoodProduct"] = list;
            return View("ShowGoodProduct");
        }
    }
}