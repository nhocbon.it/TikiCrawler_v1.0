﻿using HtmlAgilityPack;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using TikiCrawler.Models;

namespace TikiCrawler.Controllers
{
    public class GetTikiController : Controller
    {
        public void Index(string home = "http://tiki.vn")
        {
            //string home = WebConfigurationManager.AppSettings["HomePage"];
            List<string> urls = GetAllLinks(home);

            //List<string> result= new List<string>();
            //foreach(string item in urls)
            //{
            //    if (CheckLinkProductTrue(item))
            //    {
            //         if(GetProductInformations(item) == null)
            //        {
            //            result.Add(item);
            //        }

            //    }

            //}
            //ViewData["Url"] = result;
            //return View();
            List<string> url_nonproduct = new List<string>();
            foreach (string item in urls)
            {
                if (CheckLinkProductTrue(item))
                {
                    Product product = GetProductInformations(item);
                    NoSql db = new NoSql();

                    db.InsertOrUpdate(product);
                }
                else if (item.StartsWith("/"))
                {
                    url_nonproduct.Add(item);
                }
            }

            //ViewData["urls"] = urls;
            //return View();
            //string url = "http://tiki.vn/bom-dien-hut-xa-2-chieu-wenbo-intex-p217030.html";
            ////string url = "http://tiki.vn/iphone-6s-plus-64gb-p156648.html";
            //bool test = CheckLinkProductTrue(url);
            //Product pro = GetProductInformations(url);
        }

        [HttpGet]
        public ActionResult InsertProduct(string url = "http://tiki.vn")
        {
            if (CheckLinkProductTrue(url))
            {
                Product product = GetProductInformations(url);
                NoSql db = new NoSql();

                db.InsertOrUpdate(product);
            }
            ViewBag.Status = "OK";
            return View("InsertStatus");
        }

        public List<string> ProductFilter(List<string> urls)
        {
            List<string> urls_filter = new List<string>();
            foreach (string item in urls)
            {
                if (CheckLinkProductTrue(item))
                    urls_filter.Add(item);
            }
            return urls_filter;
        }

        // return true if this is product url
        public bool CheckLinkProductTrue(string url)
        {
            HtmlDocument doc = LoadHtml(url);
            if (doc == null) return false;
            return doc.DocumentNode.SelectNodes("//div[@class=\"product-customer-box\"]") != null;
        }

        // load html source from a url
        public HtmlDocument LoadHtml(string url)
        {
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();
            try
            {
                doc = hw.Load(url);
            }
            catch (Exception)
            {
                doc = null;
            }
            return doc;
        }

        // get product information 
        public Product GetProductInformations(string url)
        {
            HtmlDocument htmlSnippet = LoadHtml(url);
            Product product = new Product(); // init a new product
            try
            {
                //get product id
                try
                {
                    HtmlNode Id_Node = htmlSnippet.DocumentNode.SelectNodes("//input[@name=\"id\"]")[0];
                    HtmlAttribute Id = Id_Node.Attributes["value"];
                    product.Id = Convert.ToInt32(Id.Value);
                }
                catch (Exception) { product.Id = 0; }

                //get product name
                try
                {
                    HtmlNode Name = htmlSnippet.DocumentNode.SelectNodes("//h1[@class=\"item-name\"]")[0];
                    product.Name = Name.InnerText.Trim();
                }
                catch (Exception) { product.Name = null; }

                // get product brand
                try
                {
                    HtmlNode Brand = htmlSnippet.DocumentNode.SelectNodes("//div[@class=\"item-brand\"]")[0];
                    product.Brand = Brand.InnerText.Trim().Substring(20).Trim();
                }
                catch (Exception) { product.Brand = null; }

                //get product SKU
                try
                {
                    HtmlNode SKU_Node = htmlSnippet.DocumentNode.SelectNodes("//input[@name=\"sku\"]")[0];
                    HtmlAttribute SKU = SKU_Node.Attributes["value"];
                    product.SKU = SKU.Value;
                }
                catch (Exception) { product.SKU = null; }

                // get product price
                try
                {
                    HtmlNode Price_Node = htmlSnippet.DocumentNode.SelectNodes("//input[@name=\"price\"]")[0];
                    HtmlAttribute Price = Price_Node.Attributes["value"];
                    product.Price = Convert.ToInt32(Price.Value);
                }
                catch (Exception) { product.Price = 0; }

                // get product 
                try
                {
                    HtmlNode Category_Node = htmlSnippet.DocumentNode.SelectNodes("//input[@name=\"category\"]")[0];
                    HtmlAttribute Category = Category_Node.Attributes["value"];
                    product.Category = Category.Value;
                }
                catch (Exception) { product.Category = null; }

                try
                {
                    //get star point
                    HtmlNode TotalReviewPoint = htmlSnippet.DocumentNode.SelectNodes("//p[@class=\"total-review-point\"]")[0];
                    product.ReviewPoint = Convert.ToDouble(TotalReviewPoint.InnerText.Remove(TotalReviewPoint.InnerText.Length - 2));
                }
                catch (Exception) { product.ReviewPoint = 0; }


                // get product 
                product.Url = EditProductLink(url);
                //HtmlNode Url_Node = htmlSnippet.DocumentNode.SelectNodes("//span[@class=\"fb-comments-count\"]")[0];
                //HtmlAttribute Url = Url_Node.Attributes["data-href"];

                // get 50 page comment review
                product.Comments = GetAllComment(url, 999999999);
                //product.Comments = null;

            }
            catch (Exception) { return null; }

            return product;
        }

        public List<Comment> GetAllComment(string url, int pages)
        {
            List<Comment> result = new List<Comment>();
            for (int i = 1; i <= pages; i++)
            {
                List<Comment> comments = GetAllCommentFromSource(LoadHtml(EditProductLink(url, i)));
                if (comments.Count == 0) return result;
                result = result.Union(comments).ToList();
            }
            return result;
        }

        public List<Comment> GetAllCommentFromSource(HtmlDocument htmlSnippet)
        {
            List<Comment> comments = new List<Comment>();

            int i = 0;
            try
            {
                // get comment
                foreach (HtmlNode itemNode in htmlSnippet.DocumentNode.SelectNodes("//div[@itemprop=\"review\"]"))
                {

                    Comment comment = new Comment();
                    try
                    {
                        HtmlNode title = itemNode.SelectNodes("//p[@class=\"review\"]")[i];
                        comment.Title = title.InnerText.Trim();

                        string tempID = title.InnerHtml.Trim();
                        tempID = tempID.Remove(tempID.IndexOf(comment.Title) - 2);
                        tempID = tempID.Substring(tempID.IndexOf("nhan-xet/") + 9);
                        comment.Id = Convert.ToInt32(tempID);

                        // get comment author name
                        comment.Name = itemNode.SelectNodes("//p[@class=\"name\"]")[i].InnerText.Trim();

                        // how long comment
                        string howlong = itemNode.SelectNodes("//p[@class=\"days\"]")[i].InnerText.Trim();
                        howlong = howlong.Remove(howlong.Length - 1); // remove character ')'
                        howlong = howlong.Substring(1); // remove character '('
                        comment.Days = howlong;

                        // get comment description
                        comment.Decription = itemNode.SelectNodes("//span[@class=\"review_detail\"]")[i].InnerText.Trim();

                        // add to list comment
                        comments.Add(comment);
                        i++;
                    }
                    catch (Exception)
                    {
                        return comments;
                    }
                }
            }
            catch (Exception) { }
            return comments;
        }

        // get all link
        public List<string> ExtractAllAHrefTags(HtmlDocument htmlSnippet)
        {
            List<string> hrefTags = new List<string>();
            foreach (HtmlNode link in htmlSnippet.DocumentNode.SelectNodes("//a[@href]"))
            {
                hrefTags.Add(link.Attributes["href"].Value);
            }
            return hrefTags;
        }

        // filter all product link
        public List<string> FilterAllProduct(List<string> UrlList)
        {
            string homepage = WebConfigurationManager.AppSettings["HomePage"];
            List<string> UrlPrimary = new List<string>();
            foreach (string item in UrlList)
            {
                if (item.StartsWith(homepage) && !item.Contains("customer"))
                {
                    //check product link
                    if (CheckLinkProductTrue(item))
                        UrlPrimary.Add(item);
                }
                else if (item.StartsWith("/"))
                {
                    UrlPrimary.Add(homepage + item);
                }
            }
            return UrlPrimary;
        }

        // filter link invalid
        public List<string> FilterAll(List<string> UrlList)
        {
            string homepage = WebConfigurationManager.AppSettings["HomePage"];
            List<string> UrlPrimary = new List<string>();

            foreach (string item in UrlList)
            {
                if (item.StartsWith(homepage))
                {
                    if (!item.Contains("customer") && !item.Contains("http://tiki.vn/thuong-hieu/lowerpro.html"))
                    {
                        UrlPrimary.Add(item);
                    }
                }
                else if (item.StartsWith("/"))
                {
                    //UrlPrimary.Add(homepage + item);
                    UrlPrimary.Add(item);
                }
                else
                {
                    /*do nothing*/
                }
            }
            return UrlPrimary;
        }

        // get all link from a link
        public List<string> GetAllLinks(string url)
        {
            return FilterAll(ExtractAllAHrefTags(LoadHtml(url)));
        }

        /// <summary>
        /// add "/nhan-xet#reviewShowArea" to product link
        /// </summary>
        /// <param name="link"></param>
        /// <returns> link + "/nhan-xet#reviewShowArea"</returns>
        public string EditProductLink(string link)
        {
            return link.Remove(link.IndexOf(".html")) + "/nhan-xet#reviewShowArea";
        }
        public string EditProductLink(string link, int page)
        {
            return link.Remove(link.IndexOf(".html")) + "/nhan-xet?p=" + page + "#reviewShowArea";
        }

    }
}