﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace TikiCrawler.Models
{
    public class NoSql
    {
        public string connectionString { get; set; }
        public NoSql()
        {
            connectionString = "mongodb://root:password@54.199.247.34:27017";
        }

        public void Insert(Product product)
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("TikiData");
            var collection = database.GetCollection<Product>("Products");
            collection.InsertOneAsync(product);
        }

        public void InsertOrUpdate(Product product)
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("TikiData");

            var collection = database.GetCollection<BsonDocument>("Products");

            // insert first
            collection.InsertOneAsync(product.ToBsonDocument());

            // update
            var filter = Builders<BsonDocument>.Filter.Eq("_id", product.Id);
            var update = Builders<BsonDocument>.Update.Set("Name", product.Name)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Image", product.Image)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Category", product.Category)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Brand", product.Brand)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("SKU", product.SKU)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Price", product.Price)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("ReviewPoint", product.ReviewPoint)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            update = Builders<BsonDocument>.Update.Set("Url", product.Url)
                .CurrentDate("lastModified");
            collection.UpdateOneAsync(filter, update);

            if (product.Comments != null)
            {
                update = Builders<BsonDocument>.Update.Set("Comments", product.Comments.ToArray())
                    .CurrentDate("lastModified");
                collection.UpdateOneAsync(filter, update);
            }
        }

        public List<Product> FindAllProductByName(string name)
        {
            var client = new MongoClient(connectionString);
            var database = client.GetDatabase("TikiData");

            var collection = database.GetCollection<Product>("Products");
            var filter = Builders<Product>.Filter.Eq("Name", '/' + name + "/i");
            var result = collection.Find(filter).ToListAsync();
            //int x = result.cout
            return result.Result; 
        }

        //public BsonDocument 
        //collection.InsertOneAsync(product);
        //collection.UpdateOneAsync;
        //var updoneresult = await collection.UpdateOneAsync(
        //       Builders<Product>.Filter.Eq(Id, product.Id);
        //await Task.Run(async () =>
        //{
        //    await collection.UpdateOneAsync(a => a.Id == product.Id, Builders<Product>.Update.Set(a => a.Name, product.Name));
        //});
    }
}