﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TikiCrawler.Models
{
    public partial class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public string Category { get; set; }

        public string Brand { get; set; }

        public string SKU { get; set; }

        public int Price { get; set; }


        public double ReviewPoint { get; set; }

        public string Url { get; set; }

        public List<Comment> Comments { get; set; }

        public Product ConvertToProduct(BsonDocument bsonObject)
        {
            return BsonSerializer.Deserialize<Product>(bsonObject);
        }
    }
}