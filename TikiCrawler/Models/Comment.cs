﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TikiCrawler.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }

        public string Days { get; set; }

        public string Decription { get; set; }
    }
}